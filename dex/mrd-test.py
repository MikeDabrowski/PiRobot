from BrickPi import *

BrickPiSetup()
BrickPiSetupSensors()
BrickPiUpdateValues()
BrickPi.EncoderOffset[PORT_C] = BrickPi.Encoder[PORT_C]
power=[100] # 0 to 255
deg = [90]
port=[PORT_C]
motorRotateDegree(power,deg,port, 0, 0)