Read this in different language: [English](README.md),[Polish](README.pl.md)
***
# RaspberryPi 3 b + BrickPi + ev3dev + lego mindstorms
Ten projekt powstał na moim 3 roku informatyki. Mam dużo klocków lego i postanowiłem w końcu je wykorzystać.
***
###### Start projektu: 17.10.2016
###### Koniec projektu: ??.01.2017
***
## [BUILDLOG](BuildLog.pl.md)
***
### Ostatnia działająca konfiguracja:

* Hardware:
  - RaspberryPi  3 Model B v1.2
  - BrickPi (old model (maybe HW v1.7.3) but FW upgraded to 2.0)
  - NXT serwa i sensory
  - EV3 gyro sensor

* Ev3dev img: *ev3dev-jessie-rpi2-generic-2016-12-21.img*
* Kernel: *4.4.32-17-ev3dev-rpi2*
* Host PC: *Windows10 pro*
* IDE: *PyCharm 2016.3
* Połączenie z robotem: *wifi*
* Zmiany w config.txt (tylko odkomentowane):*
```
dtoverlay=brickpi
init_uart_clock=32000000
dtparam=brickpi_battery=okay
dtoverlay=pi3-disable-bt
dtparam=brickpi_led1_trigger=none,brickpi_led2_trigger=none
```
***
### Co znajdziesz w repozytorium
* [**dex**](dex) - pliki, które działały na raspiban for robots (DexterInd image)
 - *BrickPi.py* - cała biblioteka
 - *graykevinb_solution* - rozwiązanie problemu motorRotateDegree, nie działa
 - *gyrotest.py* - gyrotest
 - LEGO* - kolejne rozwiązanie problemu motorRotateDegree, również nie działa
 - *mrd-solution* - kolejne rozwiązanie problemu motorRotateDegree, również nie działa
 - *simplebot_simple.py* - Przykładowy program z githuba DexterIndustries
 - *test.py* - prosty test serwa

* [**ev3dev**](ev3dev) - pliki, które działaja na ev3dev na rpi3
 - [**browserbot**](ev3dev/browserbot) - Robot sterowany przez przeglądarkę. Sczegóły w readme [readme](ev3dev/browserbot/README.md)
 - [**tests**](ev3dev/tests) - Różne pliki do testów
     - *test.py* - Najprostszy test serwa
     - *simplebot1.py* - testy serw, ładowania sensorów oraz gyro, który jest zakomentowany bo nie działa
