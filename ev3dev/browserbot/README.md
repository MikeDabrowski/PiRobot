Browser Controlled Robot
========================

The following example was based on [The BrowserBot](http://www.dexterindustries.com/BrickPi/projects/browserbot/) by DexterIndustries.
I modified it to work on ev3dev and brickpi.

![My robot](WP_20161229_18_17_03_Pro_LI.jpg "")

How It Works
------------

The example uses websockets to pass messages back and forth between browser and robot.
Data is sent from the web page when you press the graphic buttons to steer the robot.
You can also control the robot with Keyboard strokes. The robot-side code runs on python using tornado.

[Video link](https://1drv.ms/v/s!AtR5ruC8xD0VgudhDcSQ3u0ntGLK8Q)
------

How To Start
------------
1. Follow [this tutorial](http://www.ev3dev.org/docs/getting-started/)
2. Setup wifi as [here](http://www.ev3dev.org/docs/tutorials/setting-up-wifi-using-the-command-line/)
3. Remember about step 3A
4. Install pip:
    1. Check if it is installed: `pip --version` and `pip3 --version`
    2. Create empty dir and enter it: `mkdir installPip && cd installPip`
    3. Download pip: `wget https://bootstrap.pypa.io/get-pip.py`
    4. Install: `sudo python get-pip.py` and `sudo python3 get-pip.py`
    5. Confirm that installation was successful with: `pip --version` and `pip3 --version`
5. Install tornado: `sudo pip install tornado && sudo pip3 install tornado`
6. Copy robot.py to rasbperry and run it
7. Copy controller.html to your device (PC or smartphone) and open it
8. On controller.html type raspberry's IP address. Rpi and your device must use the same wifi.
9. Might not start the first time because sensors need to be loaded. Close and run again and it should work.

