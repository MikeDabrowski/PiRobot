import ev3dev.brickpi as ev3

# Touch sensor 1 - port 1
p = ev3.LegoPort(ev3.INPUT_1)
p.mode = "nxt-analog"
p.set_device = "lego-nxt-touch"
ts1 = ev3.TouchSensor(ev3.INPUT_1)
m = ev3.LargeMotor(ev3.OUTPUT_B)

while not ts1.value():
	m.run_timed(time_sp=100, speed_sp=500)

