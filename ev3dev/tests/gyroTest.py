import ev3dev.brickpi as ev3
from time import sleep

p = ev3.LegoPort(ev3.INPUT_2)
p.mode = "ev3-uart"
p.set_device = "lego-ev3-gyro"

gs = ev3.GyroSensor(ev3.INPUT_2)

units = gs.units
while True:
	angle = gs.value()
	print(str(angle)+ " " + units)
	sleep(0.5)