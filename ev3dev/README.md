Few important news to start with ev3dev and BrickPi
===================================================

Import library
--------------
To import right library you need to use this command. You will be able to use it later as `ev3`:
```python
import ev3dev.brickpi as ev3
```


Ports
-----
Ev3dev.brickpi library has special constants for ports. I don't know what for are those `AUTO`. They represent 'ttyAMA0:S1' and so on.
```python
OUTPUT_A
OUTPUT_B
OUTPUT_C
OUTPUT_D
OUTPUT_AUTO

INPUT_1
INPUT_2
INPUT_3
INPUT_4
INPUT_AUTO
```

Sensors
-------
BrickPi does not autodetect sensors. You must add them manually.
[More on sensors here](http://ev3dev-lang.readthedocs.io/projects/python-ev3dev/en/stable/sensors.html)
[And here](http://www.ev3dev.org/docs/sensors/)
Certain sensors require 'ev3-uart' as mode.
In python:
```python
# Touch sensor 1 - port 1
p = ev3.LegoPort(ev3.INPUT_1)
p.mode = "nxt-analog"
p.set_device = "lego-nxt-touch"
ts1 = ev3.TouchSensor(ev3.INPUT_1)
```

Motors
------
Motors are loaded on boot and can be used right away. Max_speed returns maximal speed the motor can run at.
[More on motors here](http://ev3dev-lang.readthedocs.io/projects/python-ev3dev/en/stable/motors.html)
```python
m=ev3.LargeMotor(ev3.OUTPUT_B)
m2=ev3.LargeMotor(ev3.OUTPUT_C)
print(m.max_speed)
m.run_timed(time_sp=2000, speed_sp=1020)
m2.run_timed(time_sp=2000, speed_sp=500)
```